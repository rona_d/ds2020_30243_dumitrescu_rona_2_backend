package ro.utcn.drona.websocket;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;

/**
 * Created by Rona Dumitrescu on 17/11/2020.
 */
@Controller
public class WebSocketController {

    private final SimpMessagingTemplate template;

    @Autowired
    WebSocketController(SimpMessagingTemplate template){
        this.template = template;
    }

    @MessageMapping("/hello")
    @SendTo("/topic/greetings")
    public void sendMessage(String message){
        System.out.println(message);
        this.template.convertAndSend("/topic/greetings",  message);
    }
}
