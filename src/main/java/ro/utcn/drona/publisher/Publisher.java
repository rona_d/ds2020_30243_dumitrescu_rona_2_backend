package ro.utcn.drona.publisher;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import org.json.JSONObject;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;
import ro.utcn.drona.sensor.SensorEvent;
import ro.utcn.drona.utils.ParameterStringBuilder;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeoutException;

/**
 * Created by Rona Dumitrescu on 16/11/2020.
 */
@Component
public class Publisher implements ApplicationListener<SensorEvent> {

    private static final String EXCHANGE_NAME = "caregiverNotifications";

    private static final ConnectionFactory factory = new ConnectionFactory();
    private static Channel channel;

    public Publisher() {
        factory.setHost("localhost");
        try {
            Connection connection = factory.newConnection();
            channel = connection.createChannel();
            channel.exchangeDeclare(EXCHANGE_NAME, "fanout");

        } catch (TimeoutException | IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onApplicationEvent(SensorEvent sensorEvent) {

        // 2. daca s-a incalcat vreo regula, tre sa trimit notificare la caregiver
        // (de la ro.utcn.drona.publisher la queue)json object in metoda de trimis de la ro.utcn.drona.publisher la queue
        // 3. si sa salvez activitatea in DB

        JSONObject jsonObject = (JSONObject) sensorEvent.getSource();

        // 1) verificam daca pacientul are o problema
        String pacientProblem = hasProblem(jsonObject);

        // 2) daca are, notificam caregiveru
        if (!pacientProblem.equals("No problem")) {
            notifyCaregiver(pacientProblem, jsonObject.getInt("patient_id"));
            writePacientProblemInDB(pacientProblem, jsonObject.getInt("patient_id"));
        }

    }


    public String hasProblem(JSONObject jsonObject) {
        boolean hasProblem = false;
        String problem = "";

        // R1 Sleep period longer than 7 hours
        if (jsonObject.getString("activity").equals("Sleeping")) {
            long sleepPeriod = jsonObject.getLong("end") - jsonObject.getLong("start");
            if (sleepPeriod > (7 * 60 * 60 * 1000)) {
                hasProblem = true;
                problem = " R1, sleep period longer than 7 hours ";
            }
        }

        // R2 The leaving activity (outdoor) is longer than 5 hours
        if (jsonObject.getString("activity").equals("Leaving")) {
            long sleepPeriod = jsonObject.getLong("end") - jsonObject.getLong("start");
            if (sleepPeriod > (5 * 60 * 60 * 1000)) {
                hasProblem = true;
                problem = " R2, the leaving activity (outdoor) is longer than 5 hours ";
            }
        }

        // R3 Period spent in bathroom is longer than 30 minutes
        if (jsonObject.getString("activity").equals("Toileting") || jsonObject.getString("activity").equals("Showering")
                || jsonObject.getString("activity").equals("Grooming")) {
            long sleepPeriod = jsonObject.getLong("end") - jsonObject.getLong("start");
            if (sleepPeriod > (30 * 60 * 1000)) {
                hasProblem = true;
                problem = " R3, period spent in bathroom is longer than 30 minutes ";
            }
        }

        if (hasProblem) {
            return problem;
        } else {
            return "No problem";
        }

    }

    public void notifyCaregiver(String pacientProblem, Integer pacientID) {
        String message = "pacientID:" + pacientID + "\tpacientProblem-" + pacientProblem;

        // basic publish e trimiterea in queue defined
        try {
            channel.basicPublish(EXCHANGE_NAME, "", null, message.getBytes(StandardCharsets.UTF_8));
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println(" [x] Sent '" + message + "'");
    }

    public void writePacientProblemInDB(String pacientProblem, Integer pacientID)
    {
        try {
            URL url = new URL("https://ds2020-30243-dumitrescu-rona-1.herokuapp.com/pacient/updateMedicalRecord");
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("PUT");

            //add header
            con.setRequestProperty("Content-Type", "application/json");

            Map<String, String> parameters = new HashMap<>();
            parameters.put("id", pacientID+"");
            parameters.put("medicalRecord", pacientProblem);

            con.setDoOutput(true);
            DataOutputStream out = new DataOutputStream(con.getOutputStream());
            out.writeBytes(ParameterStringBuilder.getParamsString(parameters));
            out.flush();
            out.close();


            //read value header
           // String contentType = con.getHeaderField("Content-Type");
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


}
