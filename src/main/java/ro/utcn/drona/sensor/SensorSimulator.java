package ro.utcn.drona.sensor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;

import java.util.Timer;

/**
 *
 * Created by Rona Dumitrescu on 16/11/2020.
 */
@Component
public class SensorSimulator implements CommandLineRunner {
    @Autowired
    GeneratorSimulator generatorSimulator;

    public static final Timer timer = new Timer();

    public SensorSimulator(){}

    @Override
    public void run(String... args) {
        timer.schedule(generatorSimulator, 0, 1000);
    }
}
