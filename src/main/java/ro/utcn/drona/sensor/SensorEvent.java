package ro.utcn.drona.sensor;

import org.springframework.context.ApplicationEvent;

/**
 * Created by Rona Dumitrescu on 17/11/2020.
 */
public class SensorEvent extends ApplicationEvent {
    public SensorEvent(Object source) {
        super(source);
    }
}
