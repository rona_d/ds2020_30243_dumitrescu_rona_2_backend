package ro.utcn.drona.sensor;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;
import java.util.TimerTask;
import java.util.concurrent.TimeoutException;

/**
 * Created by Rona Dumitrescu on 17/11/2020.
 */
@Service
public class GeneratorSimulator extends TimerTask {

    @Autowired
    private ApplicationEventPublisher publisher;
    private Integer paciendID = 12;

    public GeneratorSimulator() {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");

        try (Connection connection = factory.newConnection();
             Channel channel = connection.createChannel()) {
            channel.exchangeDeclare(EXCHANGE_NAME, "fanout");

            String message = "info: Hello World!";

            // basic publish e trimiterea in queue defined
            channel.basicPublish(EXCHANGE_NAME, "", null, message.getBytes(StandardCharsets.UTF_8));
            System.out.println(" [x] Sent '" + message + "'");
        } catch (TimeoutException | IOException e) {
            e.printStackTrace();
        }
    }

    public GeneratorSimulator(Integer paciendID){
        this.paciendID = paciendID;
    }


    @Override
    public void run() {
        BufferedReader reader;
        try {
            reader = new BufferedReader(new InputStreamReader(
                    Objects.requireNonNull(GeneratorSimulator.class.getClassLoader().getResourceAsStream("activity.txt"))));


            String line = reader.readLine();
            while (line != null) {

                String[] arrayLine = line.split("\t\t");
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                Date startDate = simpleDateFormat.parse(arrayLine[0]);
                Date endDate = simpleDateFormat.parse(arrayLine[1]);
                String activityLabel = arrayLine[2];

                JSONObject jsonObject = new JSONObject();

                jsonObject.put("patient_id", paciendID);
                jsonObject.put("activity", activityLabel);
                jsonObject.put("start", new Long(startDate.getTime()));
                jsonObject.put("end", new Long(endDate.getTime()));
                SensorEvent sensorEvent = new SensorEvent(jsonObject);
                publisher.publishEvent(sensorEvent);

                System.out.println(arrayLine[0] + " StartDate  " + arrayLine[1] + " EndDate   "+ arrayLine[2]);
                // read next line
                line = reader.readLine();
            }
            reader.close();
//            SensorSimulator.timer.cancel();
//            SensorSimulator.timer.purge();
        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }
    }

    private static final String EXCHANGE_NAME = "logs";
}
